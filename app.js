const Koa = require("koa");
const Router = require("koa-router")
const json = require("koa-json");
const path = require("path");
const render = require("koa-ejs");
const bodyParser = require("koa-bodyparser");


const app = new Koa();
const router = new Router();

// import routes
const movies = require("./routers/movies")

// json middleware
app.use(json())
app.use(bodyParser())

render(app, {
  root: path.join(__dirname, "views"),
  layout: "layout",
  viewExt: "html",
  cache: false,
  debug: false
})




// routes
app.use(movies.routes())



//  app.use(async (ctx) => {
//   ctx.body = {msg: "Hello world"}
//  })



app.listen(3000);
const KoaRouter = require("koa-router");

const router = KoaRouter();

const db = ["Movie 1", "Movie 2", "Movie 3"];

router.get("/", async (ctx) => {
    await ctx.render("index", {
        title: "index",
        db: db
    });
})

router.get("/add", async (ctx) => {
    await ctx.render("add", {
        title: "add",
        db: []
    });
})

router.post("/add", async (ctx) => {
    const body = ctx.request.body;
    db.push(body.movie)
    ctx.redirect("/")
    
})

module.exports = router
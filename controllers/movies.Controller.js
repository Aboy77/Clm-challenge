// replace with DB
const db = ["Peli1", "Peli2", "Peli3"]

// list of things
async function index(ctx) {
    ctx.render("index", {
        title: "This i wanna do",
        db: db
    })
}

// add
async function showAdd(ctx) {
    await ctx.render("add", {
        title: "Search movie"
    })
}

// seach movie
async function searchMovie(ctx) {
    const body = ctx.request.body;
    db.push(body.db);
    ctx.redirect("/");
}


module.exports = {
    index,
    showAdd,
    searchMovie
}